Blast Extract
=============

Gets the BLAST entries containing any of the mentioned accessions

Usage
-----

```bash
python -m blast-extract <accession> [<accession> <accession> ...]
```

The BLAST file is read from STDIN and written to STDOUT.

```bash
python -m blast-extract HSA04115 HSA4193 <human-genes.tsv >human-p53-mdm2-genes.tsv
```

Meta
----

```ini
type=scriptlet
language=python
host=bitbucket
```
