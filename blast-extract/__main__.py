"""
Reads from std.in to std.out.
Gets the BLAST lines containing any of the mentioned sequence accessions.

Usage:
    blast-extract <accession> [<accession> <accession> ...]
"""

from sys import stdin, argv, stderr


x = set( argv[ 1: ] )

print( "Processing {} accessions...", file = stderr )

for line in stdin:
    elements = line.split( "\t", 2 )
    
    if elements[ 0 ] in x or elements[ 1 ] in x:
        print( line )

print( "All done.", file = stderr )
